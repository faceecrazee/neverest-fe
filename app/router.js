import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route("signup", function() {
    this.route("verification-sent", { path: '/verification-sent/:id' });
  });
});

export default Router;
