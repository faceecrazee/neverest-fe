import Ember from 'ember';
import DS from 'ember-data';
import Resolver from 'ember/resolver';
import loadInitializers from 'ember/load-initializers';
import config from './config/environment';

Ember.MODEL_FACTORY_INJECTIONS = true;

var App = Ember.Application.extend({
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver: Resolver
});

loadInitializers(App, config.modulePrefix);

DS.RESTAdapter.reopen({
    namespace: 'api',
    host: 'http://localhost:3000'
});

/*App.Utils = Ember.Object.extend({
    getAPIBasePath: function() {
        return "http://localhost:3000/api/";
    }
});

Ember.Application.initializer({
    name: 'utils',
    
    initialize: function(container) {
        container.register('utils:main', App.Utils, { singleton: true });
    }
});

Ember.Application.initializer({
    name: 'injectUtils',
    before: 'utils',
    
    initialize: function(container, application) {
        application.inject('controller', 'utils', 'utils:main');
        application.inject('route', 'utils', 'utils:main');
    }
});*/

export default App;
